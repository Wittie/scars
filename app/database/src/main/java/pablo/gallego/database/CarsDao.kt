package pablo.gallego.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface CarsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(entities: List<CarEntity>)

    @Query("SELECT * FROM carentity")
    fun getAll(): Flow<List<CarEntity>>

}