package pablo.gallego.database

import kotlinx.coroutines.flow.Flow

class CarsRepository(db: CarsDatabase) {

    private val carsDao = db.carsDao()

    fun getAllCars(): Flow<List<CarEntity>> = carsDao.getAll()

    suspend fun insertAll(carEntities: List<CarEntity>) = carsDao.insertAll(carEntities)

}