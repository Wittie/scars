package pablo.gallego.database

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@Suppress("MemberVisibilityCanBePrivate")
@ExperimentalCoroutinesApi
class CarsRepositoryTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    val testDispatcher = TestCoroutineDispatcher()

    var carsDaoMock = mock<CarsDao>()
    var databaseMock = mock<CarsDatabase> {
        whenever(mock.carsDao()).thenReturn(carsDaoMock)
    }

    var repository = CarsRepository(databaseMock)

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `getAllCars calls getAll in the repository`(): Unit = runBlocking {
        repository.getAllCars()
        verify(carsDaoMock).getAll()
    }

    @Test
    fun `download calls getAllCars in the insertAll`(): Unit = runBlocking {
        val carEntities = mock<List<CarEntity>>()
        repository.insertAll(carEntities)
        verify(carsDaoMock).insertAll(carEntities)
    }

}