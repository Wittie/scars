package pablo.gallego.network

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET


interface CarApiInterface {

    @GET("cars")
    suspend fun getCarsInfo(): List<Car>

    object RetrofitBuilder {

        private const val BASE_URL = "https://cdn.sixt.io/codingtask/"

        suspend fun getCarsList() = apiInterface.getCarsInfo()

        private val apiInterface by lazy {
            val okHttpClient = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            retrofit.create(CarApiInterface::class.java)
        }
    }
}
