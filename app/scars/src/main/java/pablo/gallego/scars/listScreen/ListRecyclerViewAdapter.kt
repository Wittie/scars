package pablo.gallego.scars.listScreen

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import pablo.gallego.scars.CarRepresentation
import pablo.gallego.scars.R

class ListRecyclerViewAdapter : ListAdapter<CarRepresentation, CarViewHolder>(DIFF_CALLBACK) {

    companion object {

        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<CarRepresentation>() {
            override fun areItemsTheSame(oldItem: CarRepresentation, newItem: CarRepresentation): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: CarRepresentation, newItem: CarRepresentation): Boolean {
                return oldItem.name == newItem.name &&
                        oldItem.carImageUrl == newItem.carImageUrl &&
                        oldItem.fuelLevel == newItem.fuelLevel &&
                        oldItem.colorInt == newItem.colorInt &&
                        oldItem.make == newItem.make &&
                        oldItem.modelName == newItem.modelName
            }
        }
    }

    init {
        setHasStableIds(true)
    }

    override fun getItemId(position: Int): Long = getItem(position).id.hashCode().toLong()

    override fun onCreateViewHolder(parent: ViewGroup, viewTypeRes: Int): CarViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_list_car, parent, false)
        return CarViewHolder(view)
    }

    override fun onBindViewHolder(holder: CarViewHolder, position: Int) {
        holder.onBind(getItem(position))
    }

}
