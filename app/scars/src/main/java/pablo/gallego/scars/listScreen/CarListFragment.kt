package pablo.gallego.scars.listScreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import pablo.gallego.scars.MainViewModel
import pablo.gallego.scars.R

class CarListFragment : Fragment() {

    private lateinit var mainViewModel: MainViewModel

    private lateinit var listRecyclerView: RecyclerView
    private lateinit var adapter: ListRecyclerViewAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mainViewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                mainViewModel.uiStateFlow.collect(::handleUiState)
            }
        }

        listRecyclerView = view.findViewById(R.id.listRecyclerView)
        adapter = ListRecyclerViewAdapter()

        listRecyclerView.adapter = adapter

        mainViewModel.download()
    }

    private fun handleUiState(uiState: MainViewModel.CarsUiState) {
        when (uiState) {
            MainViewModel.CarsUiState.Empty -> {}
            is MainViewModel.CarsUiState.Error -> { handleErrorState(uiState.exception) }
            MainViewModel.CarsUiState.Loading -> {}
            is MainViewModel.CarsUiState.Success -> { adapter.submitList(uiState.cars) }
        }
    }

    private fun handleErrorState(it: Throwable) {
        Toast.makeText(requireContext(), "Error: ${it.localizedMessage}", Toast.LENGTH_SHORT).show()
    }

}