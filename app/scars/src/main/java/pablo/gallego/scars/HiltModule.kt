package pablo.gallego.scars

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import pablo.gallego.database.CarsDatabase
import pablo.gallego.database.CarsRepository
import javax.inject.Qualifier
import kotlin.coroutines.CoroutineContext

@Module
@InstallIn(ActivityComponent::class, ViewModelComponent::class)
class HiltModule {


    @Provides
    @IoDispatcher
    fun providesIoDispatcher(): CoroutineContext = Dispatchers.IO

    @Provides
    @MainDispatcher
    fun providesMainDispatcher(): CoroutineContext = Dispatchers.Main

    @Provides
    fun providesDatabase(@ApplicationContext applicationContext: Context) =
        Room.databaseBuilder(applicationContext, CarsDatabase::class.java, "database-cars").build()

    @Provides
    fun providesRepository(db: CarsDatabase) = CarsRepository(db)

}


@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class IoDispatcher

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class MainDispatcher
