package pablo.gallego.scars

import android.graphics.Color
import android.util.Log
import pablo.gallego.database.CarEntity
import pablo.gallego.network.Car

object CarMapper {

    fun toEntity(car: Car) = CarEntity(
        id = car.id,
        modelIdentifier = car.modelIdentifier,
        modelName = car.modelName,
        name = car.name,
        make = car.make,
        group = car.group,
        colorInt = mapToColorInt(car.color),
        series = car.series,
        fuelType = car.fuelType,
        fuelLevel = car.fuelLevel,
        transmission = car.transmission,
        licensePlate = car.licensePlate,
        latitude = car.latitude,
        longitude = car.longitude,
        innerCleanliness = car.innerCleanliness,
        carImageUrl = car.carImageUrl,
    )

    fun toRepresentation(carEntity: CarEntity) = CarRepresentation(
        id = carEntity.id,
        modelIdentifier = carEntity.modelIdentifier,
        modelName = carEntity.modelName,
        name = carEntity.name,
        make = carEntity.make,
        group = carEntity.group,
        colorInt = carEntity.colorInt,
        series = carEntity.series,
        fuelType = carEntity.fuelType,
        fuelLevel = carEntity.fuelLevel,
        transmission = carEntity.transmission,
        licensePlate = carEntity.licensePlate,
        latitude = carEntity.latitude,
        longitude = carEntity.longitude,
        innerCleanliness = carEntity.innerCleanliness,
        carImageUrl = carEntity.carImageUrl,
    )

    private fun mapToColorInt(color: String): Int {
        return Color.parseColor(
            when (color) {
                "absolute_black_metal" -> "#EEEEEE"
                "midnight_black_metal" -> "#DDDDDD"
                "midnight_black" -> "#CCCCCC"
                "hot_chocolate" -> "#800000"
                "schwarz" -> "#FFFFFF"
                "light_white" -> "#111111"
                "iced_chocolate" -> "#D2691E"
                "alpinweiss" -> "#F0F8FF"
                "saphirschwarz" -> "#191970"
                "iced_chocolate_metal" -> "#B8860B"
                else -> {
                    Log.e("CarMapper", "Unexpected color name: $color. Returning to default black")
                    "#FFFFFF"
                }
            }
        )
    }


}