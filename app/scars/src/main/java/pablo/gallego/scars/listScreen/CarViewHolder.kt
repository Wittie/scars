package pablo.gallego.scars.listScreen

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import pablo.gallego.scars.CarRepresentation
import pablo.gallego.scars.R

class CarViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val carImageView = itemView.findViewById<ImageView>(R.id.carImageView)
    private val nameTextView = itemView.findViewById<TextView>(R.id.carNameTextView)
    private val modelTextView = itemView.findViewById<TextView>(R.id.carModelTextView)
    private val colorView = itemView.findViewById<View>(R.id.carColorView)
    private val fuelTextView = itemView.findViewById<TextView>(R.id.carFuelTextView)

    fun onBind(item: CarRepresentation) {
        nameTextView.text = item.name
        modelTextView.text = itemView.context.getString(R.string.car_model_template, item.make, item.modelName)
        colorView.setBackgroundColor(item.colorInt)
        fuelTextView.text =  itemView.context.getString(R.string.car_fuel_template, (item.fuelLevel * 100).toInt())
        carImageView.load(item.carImageUrl) {
            crossfade(true)
            placeholder(R.drawable.img_car_fallback)
            error(R.drawable.img_car_error)
        }
    }

}
