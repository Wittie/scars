package pablo.gallego.scars

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SCarsApplication : Application()