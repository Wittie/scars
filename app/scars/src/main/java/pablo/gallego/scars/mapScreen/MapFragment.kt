package pablo.gallego.scars.mapScreen

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import pablo.gallego.scars.R
import com.here.sdk.core.GeoCoordinates
import com.here.sdk.mapview.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import pablo.gallego.scars.MainViewModel


class MapFragment : Fragment() {

    private lateinit var mainViewModel: MainViewModel

    private lateinit var mapView: MapView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mainViewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)

        mapView = view.findViewById(R.id.map_view)
        mapView.onCreate(savedInstanceState)

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                mainViewModel.uiStateFlow.collect(::handleUiState)
            }
        }
    }

    private fun handleUiState(uiState: MainViewModel.CarsUiState) {
        when (uiState) {
            MainViewModel.CarsUiState.Empty -> {}
            is MainViewModel.CarsUiState.Error -> { handleErrorState(uiState.exception) }
            MainViewModel.CarsUiState.Loading -> {}
            is MainViewModel.CarsUiState.Success -> {
                val firstCar = uiState.cars.firstOrNull()
                if (firstCar != null) {
                    loadMapScene(firstCar.latitude, firstCar.longitude)
                    mapView.setOnReadyListener {
                        // This will be called each time after this activity is resumed.
                        uiState.cars.forEach { car ->
                            val markerBitmap = bitmapFromSvg(R.drawable.ic_map_marker, car.colorInt)
                            if (markerBitmap != null) {
                                val markerImage: MapImage = MapImageFactory.fromBitmap(markerBitmap)
                                val mapMarker = MapMarker(GeoCoordinates(car.latitude, car.longitude), markerImage)
                                mapView.mapScene.addMapMarker(mapMarker)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun handleErrorState(it: Throwable) {
        Toast.makeText(requireContext(), "Error: ${it.localizedMessage}", Toast.LENGTH_SHORT).show()
    }

    private fun bitmapFromSvg(@DrawableRes svgId: Int, @ColorInt colorInt: Int): Bitmap? {
        val vectorDrawable: Drawable = ResourcesCompat.getDrawable(resources, svgId, context?.theme)
            ?: return null

        val bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.setBounds(0, 0, canvas.width, canvas.height)
        DrawableCompat.setTint(vectorDrawable, colorInt)
        vectorDrawable.draw(canvas)
        return bitmap
    }

    private fun loadMapScene(latitude: Double, longitude: Double) {
        mapView.mapScene.loadScene(MapScheme.NORMAL_DAY) { mapError ->
            if (mapError == null) {
                val distanceInMeters = (1000 * 10).toDouble()
                mapView.camera.lookAt(GeoCoordinates(latitude, longitude), distanceInMeters)
            } else {
                Toast.makeText(context, "Loading map failed: mapError: " + mapError.name, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onDestroy() {
        mapView.onDestroy()
        super.onDestroy()
    }

}