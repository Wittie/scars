package pablo.gallego.scars

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import pablo.gallego.database.CarsRepository
import pablo.gallego.network.CarApiInterface
import java.lang.Exception
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: CarsRepository,
    @IoDispatcher private val ioCoroutineContext: CoroutineContext
) : ViewModel() {

    sealed class CarsUiState {
        object Empty : CarsUiState()
        data class Success(val cars: List<CarRepresentation>) : CarsUiState()
        object Loading : CarsUiState()
        data class Error(val exception: Throwable) : CarsUiState()
    }

    private val _uiStateFlow = MutableStateFlow<CarsUiState>(CarsUiState.Empty)
    val uiStateFlow: StateFlow<CarsUiState> = _uiStateFlow

    init {
        loadFromDatabase()
    }

    private fun loadFromDatabase() {
        viewModelScope.launch(ioCoroutineContext) {
            _uiStateFlow.value = CarsUiState.Loading
            repository.getAllCars()
                .catch { ex -> _uiStateFlow.value = CarsUiState.Error(ex) }
                .collect {
                    val carRepresentations = it.map(CarMapper::toRepresentation)
                    _uiStateFlow.value = CarsUiState.Success(carRepresentations)
                }
        }
    }

    fun download() {
        viewModelScope.launch(ioCoroutineContext) {
            _uiStateFlow.value = CarsUiState.Loading
            try {
                val carList = CarApiInterface.RetrofitBuilder.getCarsList()
                val entityList = carList.map(CarMapper::toEntity)
                repository.insertAll(entityList)
            } catch (ex: Exception) {
                _uiStateFlow.value = CarsUiState.Error(ex)
            }
        }
    }

}