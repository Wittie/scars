package pablo.gallego.scars

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import pablo.gallego.database.CarEntity
import pablo.gallego.database.CarsRepository

@Suppress("MemberVisibilityCanBePrivate")
@ExperimentalCoroutinesApi
class MainViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    val testDispatcher = TestCoroutineDispatcher()

    var repositoryMock = mock<CarsRepository>()

    val mainViewModel by lazy { MainViewModel(repositoryMock, testDispatcher) }

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `download calls getAllCars in the repository`(): Unit = runBlocking {
        mainViewModel.download()
        verify(repositoryMock).getAllCars()
    }

    @Test
    fun `uiStateFlow becomes CarsUiStateLoading as soon as we start`(): Unit = runBlocking {
        whenever(repositoryMock.getAllCars()).thenReturn(flow { })

        val actualFlowValue = mainViewModel.uiStateFlow.value
        val expectedFlowValue = MainViewModel.CarsUiState.Loading
        assert(actualFlowValue == expectedFlowValue)
    }

    @Test
    fun `download empty list produces a flow with CarsUiStateSuccess(empty list)`(): Unit = runBlocking {
        whenever(repositoryMock.getAllCars()).thenReturn(flow { emit(emptyList()) })

        val actualFlowValue = mainViewModel.uiStateFlow.value
        val expectedFlowValue = MainViewModel.CarsUiState.Success(emptyList())
        assert(actualFlowValue == expectedFlowValue)
    }

    @Test
    fun `download list produces a flow with CarsUiStateSuccess(list)`(): Unit = runBlocking {
        val actualValue = listOf(createCarEntity())
        whenever(repositoryMock.getAllCars()).thenReturn(flow { emit(actualValue) })

        val actualFlowValue = (mainViewModel.uiStateFlow.value as? MainViewModel.CarsUiState.Success)?.cars
        val expectedFlowValue = listOf(createRepresentation())
        assert(actualFlowValue == expectedFlowValue)
    }


    private fun createCarEntity() = CarEntity(
        id = "id",
        modelIdentifier = "modelIdentifier",
        modelName = "modelName",
        name = "name",
        make = "make",
        group = "group",
        colorInt = -1,
        series = "series",
        fuelType = "fuelType",
        fuelLevel = 0f,
        transmission = "transmission",
        licensePlate = "licensePlate",
        latitude = 0.0,
        longitude = 0.0,
        innerCleanliness = "innerCleanliness",
        carImageUrl = "carImageUrl",
    )

    private fun createRepresentation() = CarRepresentation(
        id = "id",
        modelIdentifier = "modelIdentifier",
        modelName = "modelName",
        name = "name",
        make = "make",
        group = "group",
        colorInt = -1,
        series = "series",
        fuelType = "fuelType",
        fuelLevel = 0f,
        transmission = "transmission",
        licensePlate = "licensePlate",
        latitude = 0.0,
        longitude = 0.0,
        innerCleanliness = "innerCleanliness",
        carImageUrl = "carImageUrl",
    )
}